﻿Program Task2;

//(a → b) ∧ ((a ∧ b) → ¬c)
function f(a, b, c : boolean) := (a <= b) and ((a and b) <= not c);

var FormatStrings := new List<string>;

procedure GetPermutations(InitialStr : string; ItemsSet: sequence of string);
Begin
  if ItemsSet.Count = 0
    Then FormatStrings.Add(InitialStr)
    Else foreach var Item in ItemsSet do GetPermutations(InitialStr + Item, ItemsSet.Where(x -> x <> Item));
end;

var FuncTable  := Seq
              (
              '0001',
              '0010',
              '0101',
              '0111', 
              '1001', 
              '1010',
              '1101',
              '1110'
               ).ToHashSet;

Begin 
  
  GetPermutations('', Seq('{0}', '{1}', '{2}'));
  
  foreach var FormatStr : string in FormatStrings do
    begin      
      var ResTable := new HashSet<string>;
      
      for var a := false to true do
        for var b := false to true do 
          for var c := false to true do
            begin 
              var ResStr := Format(FormatStr + '{3}', byte(a) , byte(b) , byte(c) , byte(f(a,b,c)) );        
              ResTable.Add(ResStr);
           end;   
      
        if FuncTable.SetEquals(ResTable) Then Writeln(FormatStr);        

    end; 
                
end.