﻿Program Task2; // неполная теблица истинности

{
(№ 1627) Логическая функция F задаётся выражением (w ∧ y) ∨ ((x → w) ≡ (y → z)). 
zwyx
}
//         0  1  2  3     
function f(w, x, y, z : boolean) := (w And y) Or ((x <= w) = (y <= z));


var FormatStrings := new List<string>;

procedure GetPermutations(InitialStr : string; ItemsSet: sequence of string);
Begin
  if ItemsSet.Count = 0
    Then FormatStrings.Add(InitialStr)
    Else foreach var Item in ItemsSet do GetPermutations(InitialStr + Item, ItemsSet.Where(x -> x <> Item));
end;

var FuncTable := Seq(
              '???10',
              '1??10',
              '1?110'
                ).ToHashSet;

Function CompareStrings(SampleStr, TestedStr : string) : boolean;
Begin
  Result := true;
  
  for var i := 1 to SampleStr.Length do
    Result := Result And ( (SampleStr[i] = TestedStr[i]) Or (SampleStr[i] = '?') );
  
end;

{
Надо:
Для каждой тестовой строки ищем сэмплы и для каждой пары тест - сэмплы для разных тестов должен быть хотя бы один сэмпл, отличающийся от других пар
то есть:
    
    10110 - ???10, 1??10, 1?110
    00010 - ???10
    00110 - ???10 
такой вариант не должен прокатить, так как один тест подходит под все три шаблона, а два остальных, только под первый

среди тестируемых строк должны найтись все сэмплы, при этом для каждого сэмпла должно найтись не менее одной строки, 
и количество найденных строк должно быть не меньше количества сэмплов, но этого мало, надо еще, чтобы каждому сэмплу соответствали разные строки
}
// вот этот вариант неправильный
//{2}{0}{3}{1}[00001,00101,10000,10101,00010,00110,10011,10110,01001,01101,11001,11101,01011,01111,11011,11111]
//                                     ???10 ???10       ???10
//                                                       1??10 
//                                                       1?110 
// а этот правильный
//{3}{0}{2}{1}[00001,10001,00100,10101,00010,10010,00111,10110,01001,11001,01101,11101,01011,11011,01111,11111]
//                                     ???10 ???10       ???10
//                                           1??10       1??10
//                                                       1?110                          


                
function CompareTables(SampleTable, TestedTable : HashSet<string>) : boolean;
Begin
  Result := True;
  
  var AllFoundTests := new HashSet<string>;
  
  foreach var Sample in SampleTable do 
    Begin
      var FoundTests := TestedTable.Where(test -> CompareStrings(Sample, test));
      Result := Result And (FoundTests.Count > 0); // обязательно для каждого образца должно найтись хотя бы одно соответствие
      AllFoundTests.UnionWith(FoundTests); // объединяем множества
    End;
    
    Result := Result And (AllFoundTests.Count >= SampleTable.Count); // найденных значений должно быть не меньше, чем образцов, но этого недостаточно, чтобы исключить ложные срабатывания
    
end;                

Begin 
  
  GetPermutations('', Seq('{0}', '{1}', '{2}', '{3}'));
  
  foreach var FormatStr in FormatStrings do
    begin      
      var ResTable := new HashSet<string>;
      
      for var a := false to true do
        for var b := false to true do 
          for var c := false to true do
            for var d := false to true do
              begin
                var ResStr := Format(FormatStr + '{4}', byte(a) , byte(b) , byte(c) , byte(d), byte(f(a,b,c,d)) );        
                ResTable.Add(ResStr);
           end;   
        if CompareTables(FuncTable, ResTable) Then Writeln(FormatStr, ResTable);
          
    end; 
                
end.