﻿{
(№ 1954) Петя составляет шестибуквенные слова перестановкой букв слова АВРОРА.
При этом он избегает слов с двумя подряд одинаковыми буквами. Сколько всего различных слов может составить Петя? 
}

{
всего слов - 6!
без повторения А и Р - 6!/(2! * 2!) = 180 
шаблоны:
АА????
?АА???
??АА??
???АА?
????АА

Сделаем подстановку АА = Х
слов с АА - 5! / 2! = 60 (с учетом 2х букв Р)
Сделаем подстановку РР = У
Слов с РР  - 5! / 2! = 60 (с учетом 2х букв А)
Сделаем подстановку АА = Х и РР = У
Слов с АА и РР - (6-2)! = 24

Итого: 180 - (60+60-24)  = 84

}




Function GetPermutations(Str , Letters : string ; Dict : Dictionary<string,integer>) : integer;
Begin
  Result := 0;
  if Letters = ''
    Then
      begin        
        if not ( Str.Contains('АА') Or Str.Contains('РР') ) then Dict[Str] := 1;
      end  
    Else for var i := 1 to Letters.Length do
            Result += GetPermutations(Str + Letters[i], Letters.Remove(i - 1, 1), Dict );
End;

Begin
  var Dict := new Dictionary<string,integer>;
  
  GetPermutations('', 'АВРОРА' , Dict);
  
  writeln(Dict.Count);
  
End.