﻿Program Task8;
{
(№ 223) Все 5-буквенные слова, составленные из букв П, О, Р, Т, записаны в алфавитном порядке и пронумерованы. Вот начало списка:

1. ООООО
2. ООООП
3. ООООР
4. ООООТ
5. ОООПО
...

Какое количество слов находятся между словами ТОПОР и РОПОТ (включая эти слова)? 
}

{
  Смотрим последний столбец списка:
  О П Р Т
  0 1 2 3 - четверичная система
  
  ТОПОР
  30102
  
  РОПОТ
  20103
  
  РОПОТ = 3 + 1+4*4 + 2*4*4*4*4 = 531
  ТОПОР = 2 + 1*4*4 + 3*4*4*4*4 = 786

  10,11,12,13,14,15,16,17,18,19,20 - сколько чисел между 10 и 20 включая их? 20-10+1
  
  786 - 531 + 1 = 256
} 

begin
  
  Var Letters := 'ПОРТ'.Sorted;
  Var Count := 0;
  Var FirstWordNum := 0;
  Var SecondWordNum := 0;
  
  foreach var ch1 in Letters do
    foreach var ch2 in Letters do
      foreach var ch3 in Letters do
        foreach var ch4 in Letters do
          foreach var ch5 in Letters do
              begin
                var OurWord := ch1+ch2+ch3+ch4+ch5;
                Writeln(Count, ' ', OurWord);
                If OurWord = 'ТОПОР' Then FirstWordNum := Count;
                If OurWord = 'РОПОТ' Then SecondWordNum := Count;
                Inc(Count);
              end;
  Writeln(FirstWordNum);
  Writeln(SecondWordNum);
  Writeln(Max(FirstWordNum, SecondWordNum) - Min(FirstWordNum, SecondWordNum) + 1);
end.