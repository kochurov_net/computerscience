﻿Program Task8;
{
 	(№ 3533) (Е. Джобс) Вася составляет 4-буквенные слова из букв И, Н, С, Т, А, В, К и упорядочивает их по алфавиту. При этом на первом месте может быть только согласная, на последнем - гласная. Вот начало списка:

1. ВААА
2. ВААИ
3. ВАВА
...

Укажите номер слова НИКА в этом списке. 
}

begin

  var Letters := 'ИНСТАВК'.Sorted;  // все буквы. Лень сортировать вручную - сортируем специальным методом
  var ConLetters := 'НСТВК'.Sorted; // согласные
  var SonLetters := 'ИА'.Sorted;    // гласные

  
  var Count := 0;
  foreach var ch1 in ConLetters do
    foreach var ch2 in Letters do
      foreach var ch3 in Letters do
        foreach var ch4 in SonLetters do
          begin
            Inc(Count);
            if ch1+ch2+ch3+ch4 = 'НИКА' Then Writeln(Count);
          end;
end.