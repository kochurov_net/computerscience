﻿Program Task8;
{
(№ 1903) Сколько существует чисел, шестнадцатеричная запись которых содержит 5 цифр, причём все цифры различны и никакие две чётные и две нечётные цифры не стоят рядом. 
}

{
Шестнадцатиричные цифры: 0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F - 16 цифр
Нечетные цифры: 1,3,5,7,9,B,D,F - 8 цифр
Четные цифры: 0,2,4,6,8,A,C,E - 8 цифр

В пятизначном числе четные и нечетные цифры, чтобы они не соприкасались, можно расположить следующими способами:
1. ЧНЧНЧ - три четных цифры и две нечетных
2. НЧНЧН - три нечетных цифры и две четных

Для первого числа выбираем три четных цифры из восьми: 8*7*6, но первая цифра не может быть нулем, 
следовательно, надо вычесть комбинации,где первая цифра - ноль: 8*7*6 - 1*7*6 = 7*7*6
два нечетных из восьми: 8*7, итого: 7*7*6*8*7 = 16464
Для второго числа три нечетных из восьми - 8*7*6, две четных из восьми 8*7, итого: 8*7*6*8*7 = 18816
Итого 35280

}

Begin
  
  var OddDigits := '13579BDF';
  var EvenDigits := '02468ACE';
  var EvenDigitsWithoutZero := '2468ACE';
  
  var Count := 0;
  
  // считаем ЧНЧНЧ
  foreach var ch1 in EvenDigitsWithoutZero do // четные без нуля
    foreach var ch2 in OddDigits do // нечетные
      foreach var ch3 in EvenDigits do // четные
        foreach var ch4 in OddDigits do  // нечетные
          foreach var ch5 in EvenDigits do // четные
            begin
              var OurWord : string := ch1+ch2+ch3+ch4+ch5;              
              if OurWord.Distinct.Count = 5 // вот так считаем различые буквы в строке
                then Inc(Count);
            end;

  // считаем НЧНЧН 
  foreach var ch1 in OddDigits do
    foreach var ch2 in EvenDigits do
      foreach var ch3 in OddDigits do
        foreach var ch4 in EvenDigits do
          foreach var ch5 in OddDigits do
            begin
              var OurWord : string := ch1+ch2+ch3+ch4+ch5;              
              if OurWord.Distinct.Count = 5 // вот так считаем различые буквы в строке
                then Inc(Count);
            end;

  
  Writeln(Count);
  
end.