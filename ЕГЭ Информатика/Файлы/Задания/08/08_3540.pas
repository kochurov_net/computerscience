﻿Program Task24;
{
(№ 3540) (Е. Джобс) Женя составляет слова переставляя буквы З, А, П, И, С, Ь. 
Сколько слов может составить Женя, если известно, что Ь не может стоять на первом месте и после гласной? 
}

{
  1. Полное количество перестановок букв = 6*5*4*3*2*1 = 6! = 720
  2. Количество запрещенных перестановок с Ь в начале: 1*5*4*3*2*1 = 5! = 120
  3. Количество запрещенных перестановок с АЬ (Ь в начале слова уже отброшен) АЬ???? ?АЬ???, ??АЬ??, ???АЬ?, ????АЬ (5 вариантов)
  4. В каждом из этих вариантов буквы, стоящие на месте знаков ? могут быть выбраны из четырех оставшихся букв: 4*3*2*1 = 4! = 24
  5. Общее количество запрещенных вариантов будет 5*24 = 120
  6. Аналогично считаем запрещенные перестановки с ИЬ = 120
  7. Итого: 720 - 120 - 120 - 120 = 360
}

function StrIsValid(Str : string) := Not (Str.StartsWith('Ь') Or Str.Contains('АЬ') Or Str.Contains('ИЬ'));

Function CountPermutaions(Str: string; Letters: sequence of char) : integer;
Begin
  Result := 0;
  If Letters.Count = 0
    then
        Begin
          writeln(Str);
          if StrIsValid(Str) Then Result := 1
        end       
    else 
      Foreach var ch in Letters do 
        Result += CountPermutaions(Str + ch, Letters.Where(x-> x <> ch) );
end;

Begin

  Writeln(CountPermutaions('', 'ЗАПИСЬ'));
  
end.