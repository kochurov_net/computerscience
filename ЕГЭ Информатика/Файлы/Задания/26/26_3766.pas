﻿Program Task26;
{
(№ 3766) (А. Кабанов) В текстовом файле записан набор натуральных чисел. Гарантируется, что все числа различны. Рассматриваются пары с чётной суммой, такие что:
- хотя бы половина чисел набора меньше среднего арифметического пары
- хотя бы четверть чисел набора больше среднего арифметического пары 
Определите количество таких пар и наименьшее из средних арифметических таких пар.
Входные данные представлены в файле 26-50.txt следующим образом. Первая строка содержит целое число N – общее количество чисел в наборе. 
Каждая из следующих N строк содержит одно число, не превышающее 10^9. 
В ответе запишите два целых числа: сначала количество пар, затем наименьшее среднее арифметическое.
}

Begin
  
  Assign(input,'26-50.txt');
  Var NumCount := ReadInteger; // прочитали количество чисел
  Var AllNums := ReadSeqInteger(NumCount).Sorted.ToList; // прочитали сами числа из файла
                                                         // сортируем, иначе поиск не будет работать
  
  var AvgList := new List<integer>;

  Var BorderLeft := AllNums[(AllNums.Count div 2) -1];  // вычисляем значение в середине сортированного массива. 
                                                         // Если среднее значение больше этого числа, то оно больше половины значений массива
  Var BorderRight := AllNums[AllNums.Count * 3 div 4];  // значение в 3/4 сортированного массива                                                      
  
  for var i := 0 to AllNums.Count - 2 do
      for var j := i + 1 to AllNums.Count - 1 do
        if (AllNums[i] + AllNums[j]) mod 2 = 0
          Then 
            begin
              Var avg  := (AllNums[i] + AllNums[j]) div 2;
              if (BorderLeft < avg) And (avg < BorderRight) Then AvgList.Add(avg);
              //if (AllNums.Where(x -> x < avg).Count >= AllNums.Count / 2) And (AllNums.Where(x -> x > avg).Count >= AllNums.Count / 4) Then AvgList.Add(avg); // очень долго работает
            end;

  writeln(AvgList.Count, ' ', AvgList.Min);
  
  //MillisecondsDelta.Println; // показывает время работы программы
 
end.

