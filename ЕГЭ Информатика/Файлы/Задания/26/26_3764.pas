﻿Program Task26;
{
(№ 3764) (А. Кабанов) В текстовом файле записан набор натуральных чисел. Гарантируется, что все числа различны. 
Для каждой пары различных чисел из набора с чётной суммой вычисляется значение K – наименьшая разница между средним арифметическим пары и каким-либо числом из набора. 
Необходимо определить количество пар чисел, для которых значение K равно 5, а также наименьшее из средних арифметических таких пар.
Входные данные представлены в файле 26-48.txt следующим образом. 
Первая строка содержит целое число N – общее количество чисел в наборе. Каждая из следующих N строк содержит одно число, не превышающее 10^9. 
В ответе запишите два целых числа: сначала количество пар, затем наименьшее среднее арифметическое.
}

{
with open('26-48.txt') as F:
  N = int(F.readline())
  data = [int(s) for s in F]

data.sort()

averages = []
for i in range(N-1):
  for j in range(i+1,N):
    s = data[i] + data[j]
    if s % 2 == 0:
      averages.append( (data[i]+data[j])//2 )
averages.sort()

count, mi = 0, 10**10
i = 0
for avg in averages:
  while i < N and data[i] < avg:
    i += 1
  k = min( avg-data[i-1], data[i]-avg )
  if k == 5:
    count += 1
    mi = min(mi, avg)

print(count, mi)
}

{
Про число К автор хотел сказать вот что. Надо найти в сортированном наборе два ближайших к среднему числа - меньше среднего и следующее за ним число. 
Теперь из большего числа надо вычесть среднее и из среднего вычесть меньшее. Из получившихся чисел взять минимальное.
В общем, не от натурала задача
}

Begin
  
  Assign(input,'26-48.txt');
  Var NumCount := ReadInteger; // прочитали количество чисел
  Var AllNums := ReadArrInteger(NumCount).Sorted.ToList; // прочитали сами числа из файла
  AllNums.Sort(); // сортируем, иначе поиск не будет работать
  
  var AvgList := new List<integer>;
  
  for var i := 0 to AllNums.Count - 2 do
      for var j := i + 1 to AllNums.Count - 1 do
        if (AllNums[i] + AllNums[j]) mod 2 = 0
          Then 
            begin
              Var avg  := (AllNums[i] + AllNums[j]) div 2;
              var avg_i := AllNums.FindIndex(i, x-> x > avg); // находим индекс первого числа, которое больше среднего
                                                              // это число у нас будет AllNums[avg_i]   
                                                              // а предыдущее AllNums[avg_i - 1] 
                                                              // искать начинаем с позиции i, потому что у нас массив отсортирован,
                                                              // и среднее не может быть меньше минимального слагаемого
              if avg_i < 1 Then Continue; // если -1 то не найдено, а еще надо будет единицу из индекса вычитать, так что < 1                                                              
              If Min(AllNums[avg_i] - avg, avg - AllNums[avg_i - 1]) = 5 Then AvgList.Add(avg);
              
            end;

  writeln(AvgList.Count, ' ', AvgList.Min);
  
  //MillisecondsDelta.Println; // показывает время работы программы
 
end.


