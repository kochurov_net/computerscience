﻿Program Task26;
{
(№ Trunc) (Е. Джобс) В магазине проводят акция – каждый второй товар со скидкой 50%. 
При этом в акции участвуют только те товары, цены которых попадают в одну ценовую категорию. 
Каждая ценовая категория включает 500 целых значений: 1-500, 501-1000, 1001-1501 и т.д. 
Например, при наличии в чеке только позиций с ценами 300 и 1000 предложение акции не работает. 
Необходимо распределить товары в чеке таким образом, чтобы итоговая цена всех товаров была максимально выгодной для магазина. 
В качестве ответа вывести полученную сумму скидки для всего чека и конечную стоимость самого дорогого проданного по акции товара. 
В случае получения нецелых значений привести только целые части найденных чисел.
Входные данные представлены в файле 26-44.txt следующим образом. 
В первой строке записано натуральное число N – количество покупаемых товаров (10 ≤ N ≤ 10000). 
В следующих N строках находятся значения – стоимость каждого товара (все числа натуральные, не превышающие 10 000), по одному в каждой строке.
}

{
Идея такая: Выбирать товары парами - сначала самый дорогой, а потом самый дорогой из других ценовых категорий, таким образом, скидка на второй товар не будет предоставляться
Идея оказалась не совсем правильная - второй товар надо брать не из ближайшей ценовой категории, из второй половины списка
Эта идея тоже оказалась неправильная - еcли так расставлять, то скидка вообще не начислится
Оказывается, автор имел в виду следующее: надо разбить сортированный список на части, чтобы в каждой были собраны товары из одной ценовой группы, 
а уж потом эти группы делить пополам, к сумме меньшей половины применяем скидку (сомнительное решение в связи с округлением), 
и макcимальный элемент из минимальной половины берем (после применения скидки) в качестве кандидата на максимальную цену
}
Begin
  
  Assign(input,'26-44.txt');
  Var NumCount := ReadInteger; // прочитали количество чисел
  Var AllNums := ReadArrInteger(NumCount); // прочитали сами числа из файла

  var MaxPrice := 0.0;
  var Discount := 0.0;

  Var Category := 0;
   
  While true do
    Begin
      var Goods := AllNums.Where(x -> (x > Category * 500) and (x <= (Category + 1) * 500)).Sorted.ToArray();
      Inc(Category);
      If Goods.Count = 0 Then Break;
      
      var MidIndex := (Goods.Count div 2) - 1 + 1 ; // получаем индекс начала второй половины массива
      //Discount += Goods[:MidIndex].Sum / 2; // берем срез массива от 0 до MidIndex (не включая его), получаем сумму элементов массива и применяем скидку
      Discount += Goods.Take(MidIndex).Sum / 2; // берем срез массива от 0 до MidIndex (не включая его), получаем сумму элементов массива и применяем скидку
      MaxPrice := Max(MaxPrice, (Goods[MidIndex - 1] / 2)); // последний элемент первой половины массива
    end;

  writeln(Trunc(Discount), ' ', Trunc(MaxPrice));
  
  //MillisecondsDelta.Println; // показывает время работы программы
 
end.


