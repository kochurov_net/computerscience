﻿Program Task26;
{
(№ 3770) В текстовом файле записан набор натуральных чисел. Гарантируется, что все числа различны. 
Необходимо определить, сколько в наборе таких пар чётных чисел, что их среднее арифметическое тоже присутствует в файле, и чему равно наименьшее из средних арифметических таких пар.
Входные данные представлены в файле 26-53.txt следующим образом. 
Первая строка содержит целое число N – общее количество чисел в наборе. Каждая из следующих N строк содержит одно число, не превышающее 10^9. 
В ответе запишите два целых числа: сначала количество пар, затем наименьшее среднее арифметическое.
}

Begin
  
  Assign(input,'26-53.txt');
  Var NumCount := ReadInteger; // прочитали количество чисел
  Var AllNums := ReadSeqInteger(NumCount).Sorted.ToList; // прочитали сами числа из файла
                                                         // сортируем, иначе поиск не будет работать
                                                         // и по этой же причине преобразуем в List
  
  var EvenNums := AllNums.Where(x -> x mod 2 = 0).Sorted.ToArray(); // отфильтровали четные числа, отсортировали их и сложили в массив
  
  var AvgList := new List<integer>;
  
  for var i := 0 to EvenNums.Count - 2 do
      for var j := i + 1 to EvenNums.Count - 1 do
        begin
          Var avg  := (EvenNums[i] + EvenNums[j]) div 2;
          //if AllNums.FindIndex(i, x -> x = avg) > 0 Then AvgList.Add(avg); // ищет, но медленно
          if AllNums.BinarySearch(avg) > 0 Then AvgList.Add(avg); // ищет быстро, но массив должен быть обязательно отсортирован
        end;

  writeln(AvgList.Count, ' ', AvgList.Min);
  
  //MillisecondsDelta.Println; // показывает время работы программы
 
end.