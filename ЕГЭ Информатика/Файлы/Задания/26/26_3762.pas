﻿Program Task26;
{
(№ 3762) (А. Кабанов) В текстовом файле записан набор натуральных чисел. Гарантируется, что все числа различны. 
Необходимо определить, сколько в наборе троек чисел с суммой, кратной трём, таких что их среднее арифметическое тоже присутствует в файле, 
и чему равно наименьшее из средних арифметических таких троек.
Входные данные представлены в файле 26-46.txt следующим образом. Первая строка содержит целое число N – общее количество чисел в наборе. 
Каждая из следующих N строк содержит одно число, не превышающее 10^9. 
В ответе запишите два целых числа: сначала количество троек, затем наименьшее среднее арифметическое.
}

Begin
  
  Assign(input,'26-46.txt');
  Var NumCount := ReadInteger; // прочитали количество чисел
  Var AllNums := ReadArrInteger(NumCount); // прочитали сами числа из файла
  Sort(AllNums); // сортируем,иначе поиск не будет работать
  
  var AvgList := new List<integer>;
  
  for var i := 0 to High(AllNums) - 2 do // AllNums.High = AllNums.Length - 1 это просто верхняя граница массива 
    for var j := i + 1 to High(AllNums) - 1 do
      for var k := j + 1 to High(AllNums) do
        if (AllNums[i] + AllNums[j] + AllNums[k]) mod 3 = 0
          Then
            begin
              var avg := (AllNums[i] + AllNums[j] + AllNums[k]) div 3;
              //If AllNums.BinarySearch(avg) >= 0 Then AvgList.Add(avg);              
              If &Array.BinarySearch(AllNums, avg) >= 0 Then AvgList.Add(avg); // хрен найдешь функцию, но я справился!
                                                                               // есть вариант преобразовать массив в список в самом начале,
                                                                               // и будет работать закомментированная строчка, но
                                                                               // тогда надо будет High на Count - 1 менять
            end;
  
  writeln(AvgList.Count, ' ', AvgList.Min);
  
  //MillisecondsDelta.Println; // показывает время работы программы
 
end.


