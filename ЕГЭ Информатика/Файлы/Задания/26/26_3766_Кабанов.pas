﻿// Автор: А.М. Кабанов

function bisect_left(a: array of integer; x:real):integer;
begin
  //var (lo, hi):=(0, a.High);
  var lo := 0;
  var hi := a.Length - 1;
  while lo<hi do begin
    var mid:= (lo+hi)div 2;
    if a[mid]<x then lo:=mid+1 else hi:=mid;
  end;
  Result:=lo;
end;

begin
 var f:= openRead('26-50.txt');
 var n:= f.ReadInteger();
 var a:= Range(1,n).Select(i->f.ReadInteger()).Sorted.ToArray;
 
 var count := 0;
 var m := Longint.MaxValue;
 
 for var i:=0 to a.Length-2 do
   for var j:=i+1 to a.Length-1 do
     if (a[i]+a[j]) mod 2 = 0 then begin
       var x:=(a[i]+a[j]) div 2;
       var pos1:=bisect_left(a,x);
       var pos2:=pos1;
       if x < a[pos2] then pos2 -= 1;
       if (pos1 > a.Count div 2 - 1) and (pos2 < a.Count div 4 *3) then begin
         count:=count+1;
         m:=min(m, x);
       end;
     end;
print(count,m);
end.

{
with open('26-50.txt') as F:
  N = int(F.readline())
  data = []
  for i in range(N):
    data.append( int(F.readline()) )
  #data = [int(s) for s in F]

data.sort()

averages = []
for i in range(N-1):
  for j in range(i+1,N):
    s = data[i] + data[j]
    if s % 2 == 0:
      #print( data[i], data[j], s//2 )
      averages.append( s//2 )
averages.sort()

borderLeft = data[N//2-1]
borderRight = data[3*N//4]
selected = [x for x in averages
              if borderLeft < x < borderRight];

print( len(selected), selected[0] )

}