﻿Program Task27;
(*
(№ 3776) В файле записана последовательность натуральных чисел. Гарантируется, что все числа различны. 
Из этой последовательности нужно выбрать четыре числа, чтобы их сумма делилась на 9 и была наименьшей. 
Какую наименьшую сумму можно при этом получить?
Входные данные. Даны два входных файла (файл A и файл B), каждый из которых содержит в первой строке количество чисел N (1 ≤ N ≤ 100000). 
Каждая из следующих N строк содержит одно натуральное число, не превышающее 10^8.
Пример входного файла:
6
5
7
12
23
2
8
Для указанных данных можно выбрать четвёрки 5, 12, 2, 8 (сумма 27) и 12, 23, 2, 8 (сумма 45). Наименьшая из сумм – 27. 
В ответе укажите два числа: сначала искомое значение для файла А, затем для файла B. 
*)

Begin 
  assign(input, '27-57a.txt');
  var AllNums := Range(1,ReadLnInteger).Select(x -> ReadlnInteger());
  
  //Var MinNums := ArrFill(9,  new List<integer>); // АХТУНГ: вот так писать нельзя - ссылка во всех элементах будет на один и тот же список
  Var MinNumsArr := Range(0,8).Select(x -> New List<integer>).ToArray; // а так вот можно - создаем массив с индексами 0..9 (остатки от деления на 9) и заполняем его элементы пустыми списками
    
  foreach var Num in AllNums do
    begin
      var Index := Num mod 9; // вычисляем индекс списка в массиве
      var CurrList := MinNumsArr[Index]; // получаем ссылку на список в массиве по индексу
      if (CurrList.Count = 0) Or (CurrList.Any(x -> Num < x)) // если список пустой, либо наше число меньше одного из значений в нем
        Then 
          begin
            CurrList.Add(Num); // добавляем число в список
            if CurrList.Count = 5 Then CurrList.Remove(CurrList.Max); // если после добавления величина списка равна 5, то выкидываем из него максимальный элемент,
          end;                                                        // таким образом у нас в массиве 9 списков и в каждом не более 4 элементов
    end;

    // теперь у нас есть массив списков чисел, индекс массива - остаток от деления на 9, внутри массива списки, содержащие по 4 числа
    // нам сейчас надо сначала просуммировать список с индексом 0 - сумма точно разделится на 9
    // а дальше надо суммировать числа из разных списков так, чтобы сумма делилась на 9
    // но надо перебирать сочетания - четыре числа можно собрать из количества списков от одного до четырех,
    // поэтому лучше собрать все числа в один список и перебрать сочетания по 4
    
    var MinNums := new List<integer>; // создаем пустой список
    foreach var List4 in MinNumsArr do MinNums.AddRange(List4); // перекладываем в него значения из списков, которые хранятся в массиве    

    Var Sums := new List<integer>; // сюда будем суммы складывать
        
    for var i := 0 + 0  to MinNums.Count - 1 - 3 do 
      for var j := i + 1 to MinNums.Count - 1 - 2 do 
        for var k := j + 1 to MinNums.Count - 1 - 1 do 
          for var l := k + 1 to MinNums.Count - 1 - 0 do
            begin
              if Arr(i,j,k,l).Distinct.Count < 4 Then Continue; // на всякий случай убираем одинаковые индексы
              Sums.Add( (Arr(MinNums[i], MinNums[j], MinNums[k], MinNums[l])).Sum );
            End;
    
   Println(Sums.Where(x -> x mod 9 = 0).Min); 

{   но поскольку у нас в 2.2 нельзя получить сочетания, придется пойти другим путем

    var MinNums := new List<integer>; // создаем пустой список
    //foreach var List4 in MinNumsArr do MinNums += List4; // перекладываем в него значения из списков, которые хранятся в массиве
    foreach var List4 in MinNumsArr do MinNums.AddRange(List4); // перекладываем в него значения из списков, которые хранятся в массиве
    // у нас получился в результате список минимальных чисел,  не более 4, остаток от деления на 9 которых равен нулю, не более 4, с остатком 1 итд
    // то есть не более 4*9=36 чисел в списке, следовательно, не более 58905 сочетаний придется нам перебрать
    MinNums.Combinations(4).Select(x -> x.Sum).Where(x -> x.Divs(9)).Min.Println; 
    // 1. получаем сочетания по 4 числа из списка
    // 2. считаем суммы полученных списоков
    // 3. отбираем те суммы, которые делятся на 9
    // 4. из полученных сумм определяем минимальную
    // 5. печатаем ее
}
end.