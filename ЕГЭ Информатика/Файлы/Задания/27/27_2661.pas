﻿{
(№ 2661) Имеется набор данных, состоящий из пар положительных целых чисел. 
Необходимо выбрать из каждой пары ровно одно число так, чтобы сумма всех выбранных чисел не делилась на 3 и при этом была минимально возможной. 
Гарантируется, что искомую сумму получить можно. Программа должна напечатать одно число – минимально возможную сумму, соответствующую условиям задачи.
Входные данные. Даны два входных файла (файл A и файл B), каждый из которых содержит в первой строке количество пар N (1 ≤ N ≤ 100000).
 Каждая из следующих N строк содержит два натуральных числа, не превышающих 10 000.
Пример входного файла:

6
1 3
5 12
6 9
5 4
3 3
1 1

Для указанных входных данных значением искомой суммы должно быть число 20.
В ответе укажите два числа: сначала значение искомой суммы для файла А, затем для файла B. 
}

Begin
  Assign(input, '27-1a.txt');
  var AllPairs := Range(1,ReadLnInteger).Select(x -> ReadArrInteger(2)).ToArray; // читаем пары чисел из файла в массивы из двух целых, и складываем их в результируюющий массив
  
  var MinSum := AllPairs.Select(x -> x.Min()).Sum(); // вычисляем сумму минимальных чисел из каждой пары

  if MinSum mod 3 <> 0
    Then PrintLn(MinSum) // если сумма не делится на 3, то ничего делать не надо, просто печатаем ее
    Else
      begin
        // в противном случае выбираем получаем из из каждой пары чисел их разность по модулю, отбираем такие, которые не делятся на 3 и выбираем из них минимальную
        var MinDiff := AllPairs.Select(x -> Abs(x[1] - x[0])).Where(x -> x mod 3 <> 0).Min;
        Writeln(MinSum + MinDiff); // эту минимальную разность прибавляем к ранее вычисленной сумме
      end;

  Writeln('----------------------');
  
  var Sums := Arr(0,0,0); // Arr(3, 0); // массив сумм с остатками 0,1,2 (индекс массива) заполненный нулями
  for var i := 0 to AllPairs.Count - 1 do // цикл по всем парам в массиве, счетчик нужен для определения первого прохода
    begin
      var CurSums := Arr(integer.MaxValue, integer.MaxValue, integer.MaxValue); // массив временых сумм по остаткам, заполняем максимальными значениями
      
      foreach var Sum in Sums do // перебираем все суммы (3 штуки)
        foreach var Num in AllPairs[i] do // перебираем числа в парах (2 штуки)     
          begin

            var CurSum := Num + Sum; // вычисляем сумму числа из пары и ранее полученной суммы
            var Index := CurSum mod 3; // вычисляем индекс для массива
            // если у нас вычисленное значение суммы меньше ранее полученного значения в массиве с соответствующим индексом, то сохраняем его в массиве
            // проверка происходит либо при первом проходе (при обработке первой пары),
            // либо когда ранее полученная сумма не равна нулю
            // сделано так потому, что в каждом элементе массива должна быть сумма одинакового количества чисел,
            // поэтому первая пара (точнее сумма этих чисел с нулем) попадает в массив Sums (в одну или в две ячейки, в зависимости от делимости чисел),
            // а следующие пары уже с нулями суммировать нельзя, они суммируются только с ненулевыми значениями
            if ( (i = 0) Or (Sum <> 0)) and (CurSum < CurSums[Index]) Then CurSums[Index] := CurSum;
            
          end;
      // копируем полученные значения CurSums в массив Sums за исключением значений integer.MaxValue - это значит, что значения с таким остатком нет для этой пары  
      for var j:=0 to Sums.Count - 1 do if CurSums[j] <> integer.MaxValue Then Sums[j] := CurSums[j];
      

    end;
  
  Println(Min(Sums[1], Sums[2])); // выводим минимальное значение из массива с индексами (то есть с остатком от деления на 3) 1 и 2
 
  Writeln('----------------------');
  
  Sums := Arr(0,0,0); // Arr(3, 0);
  
  foreach var Pair in AllPairs do // перебираем пары
    begin
      
      var CurSums := new List<integer>; // суммы будем хранить в списке - 3*2 элементов может быть максимально 
      
      foreach var Sum in Sums do // перебираем суммы
        foreach var Num in Pair do if Sums.All(x -> x = 0) or (Sum <> 0) Then CurSums.Add(Sum + Num); // перебираем пары, и добавляем такие, где либо первый проход, либо сумма ненулевая
      
      foreach var Index in Range(0,2) do // раскладываем полученные суммы в массив Sums по индексу
        begin
          var PairsSums := CurSums.Where(x -> x mod 3 = Index );
          if PairsSums.Count > 0 Then Sums[Index] := PairsSums.Min;
        end;

    end;
  
  //Sums[1:].Min.Println; // можно и так сделать - взять срез массива без индекса 0, получить среди остальные элементов минимальный и напечатать его
  Println(Sums.Skip(1).Min); // нету срезов в 2.2
  
  Writeln('----------------------');
  
  // самый навороченный способ. Автор - Alex Danov
  // в 2.2 не завезли :(
{
  AllPairs.Aggregate( Arr(0), 
      (a,x)-> a.Cartesian(x, (a,b)->a+b) // построить все комбинации сумм
               .GroupBy(x->x mod 3) // сгруппировать по остаткам от деления на B
               .Select(x->x.Min)    // из каждой группы выбрать минимальное
               .ToArray)
    .Where(x->not x.Divs(3))
    .Min
    .Print;      
}
  
end.
