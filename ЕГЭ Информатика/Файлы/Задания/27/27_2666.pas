﻿Program Task27;
{
(№ 2666) Имеется набор данных, состоящий из положительных целых чисел, каждое из которых не превышает 1000. 
Требуется найти для этой последовательности контрольное значение – наибольшее число R, удовлетворяющее следующим условиям:
– R – произведение двух различных переданных элементов последовательности 
(«различные» означает, что не рассматриваются квадраты переданных чисел, произведения различных, но равных по величине элементов допускаются);
– R делится на 6.
Входные данные. Даны два входных файла (файл A и файл B), каждый из которых содержит в первой строке количество чисел N (1 ≤ N ≤ 100000). 
Каждая из следующих N строк содержит одно натуральное число, не превышающее 10 000.
Пример входного файла:
6
60
17
3
7
9
60
Для указанных входных данных искомое контрольное значение равно 3600.
В ответе укажите два числа: сначала контрольное значение для файла А, затем для файла B. 
}

Begin

  Assign(input, '27-6b.txt');
  var NumCount := ReadlnInteger; // читаем количество чисел в первой строке
  var AllNums := Range(1,NumCount).Select(x-> ReadlnInteger()).ToArray; // читаем числа в массив
 
  var MaxR := 0;
  
  for var i := 0 to AllNums.Count - 2 do
    for var j := i + 1 to AllNums.Count - 1 do
      begin
        var R := AllNums[i] * AllNums[j];
        if R mod 6 <> 0 Then Continue;
        MaxR := Max(R, MaxR);
      end;
  
  Println(MaxR);
  Println(MillisecondsDelta);
  Writeln('----------------------');
  
{ Combinations не завезли в 2.2 
  
  AllNums.Combinations(2).Select(x -> x[0] * x[1]).Where(x -> x.Divs(6)).Max.Println;
  MillisecondsDelta.Println();
  
  Writeln('----------------------');

}  
  {
  Можно попробовать ускорить поиск следующим образом, исключив полный перебор:
  на 6 будут делиться произведения следующих чисел:
  1. Максимальное среди чисел, делящееся на 6 и любое ДРУГОЕ максимальное число
  2. Максимальное среди чисел делящихся на 3 и ДРУГОЕ максимальное число, делящееся на 2
  Из этих двух чисел надо выбрать максимальное
  Тонкость - надо исключать одно число при выборе, а не все числа, потому что у нас нет в условии, что все числа различные
  }
  
  Var Num_1 := AllNums.Max;
  var RemainNum_1 := AllNums.Count(x -> x = Num_1) > 1; // если у нас больше одного числа со значение Num_1, то не будем его исключать при поиске второго множителя
  var Num_6 := AllNums.Where(x -> (x mod 6 = 0) and ( RemainNum_1 Or (x <> Num_1) ) ).Max;
  
  var Num_2 := AllNums.Where(x -> x mod 2 = 0 ).Max;
  var RemainNum_2 := AllNums.Count(x -> x = Num_2) > 1; // если у нас больше одного числа со значение Num_2, то не будем его исключать при поиске второго множителя
  var Num_3 := AllNums.Where(x -> (x mod 3 = 0) and (RemainNum_2 Or (x <> Num_2)) ).Max;
  
  Writeln(Max(Num_1 * Num_6, Num_2 * Num_3));
  
  Println(MillisecondsDelta);
  
end.