﻿Program Test14; //Раздел № 61: Уравнения с данными в различных системах счисления

function StrToNum(Str : string; Base : integer) : integer;
Begin
  var Digits := '0123456789ABCDEF';
  Result := 0;
  for var i:= 1 to Str.Length do // в первом символе хранится самая старшая цифра в строке
    Begin
      Result *= Base;
      Result += Digits.IndexOf(Str[i]); // непонятно, почему IndexOf для первого символа выдает нулевую позицию, итд, но так работает
    End;  
end;

function NumToStr(Num: integer; Base: integer) : string;
Begin
  var Digits : string := '0123456789ABCDEF';
  Result := '';
  Repeat
    Result := Digits[(Num Mod Base) + 1] + Result; // добавляем цифры в начало числа, +1 - потому что символы в строке Digits нумеруются с 1
    Num := Num Div Base; // сдвигаем вправо на один разряд
  until Num = 0;
end;

Begin
{
(№ 3558) Решите уравнение 101x + 13 = 101x+1. Ответ запишите в десятичной системе счисления. 
}  
  
  For var Base:= 2 to 15 do If StrToNum('101', Base) + 13 = StrToNum('101', Base + 1) Then Writeln(Base);
  writeln('------------------');
  
{
(№ 329) Решите уравнение  60(8) + x = 120(7) Ответ запишите в шестеричной системе счисления. Основание системы счисления указывать не нужно. 
}  
  var x : integer := StrToNum('120', 7) - StrToNum('60', 8);
  Writeln(NumToStr(x, 6));
  writeln('------------------');

end.