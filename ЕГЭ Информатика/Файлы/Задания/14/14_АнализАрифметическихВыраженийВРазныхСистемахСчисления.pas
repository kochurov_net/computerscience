﻿Program Task14; //Раздел № 62: Анализ арифметических выражений в разных системах счисления


Begin
{
(№ 3600) Сколько единиц в двоичной записи числа 8^1023 + 2^1024 – 3? 
}
  Begin  
    var Num: BigInteger := Power(BigInteger(8), 1023) + Power(BigInteger(2), 1024) - 3;
    var NumOfOnes := 0;
    repeat
      If Odd(Num) Then Inc(NumOfOnes);
      Num := Num div 2; // сдвигаем на один двоичный разряд вправо
    until Num.IsZero;
    
    writeln(NumOfOnes);
  End;  

{
(№ 3581) Сколько значащих нулей в двоичной записи числа 4^350 + 8^340 – 2^320 – 12? 
}
  Begin
    writeln('------------------------');
    var Num : BigInteger := Power(BigInteger(4), 350) + Power(BigInteger(8), 340) - Power(BigInteger(2), 320) - 12;
    var NumOfZeroes := 0;
    repeat
      If Not Odd(Num) Then Inc(NumOfZeroes);
      Num := Num div 2; // сдвигаем на один двоичный разряд вправо
    until Num.IsZero;
    
    writeln(NumOfZeroes);    
  End;
  
{
(№ 3561) (Е.А. Мирончик) Какая первая цифра в шестнадцатеричной записи числа 2^1024 + 2^1025? 
}  
  Begin
    writeln('------------------------');

    var Num : BigInteger := Power(BigInteger(2), 1024) + Power(BigInteger(2), 1025);

    while Num > BigInteger(15) do Num := Num div 16; // сдвигаем на один шестнадцатиричный разряд вправо
    
    writeln(Num);    
  End;
  
{
(№ 3511) (Е. Джобс) Значение выражения 5^2004 – 5%1016 – 25^508 – 5^400 + 25^250 – 27 записали в пятеричной системе счисления. Сколько цифр 4 в такой записи? 
}

  Begin
    writeln('------------------------');

    var Num : BigInteger := Power(BigInteger(5), 2004) - Power(BigInteger(5), 1016)  - Power(BigInteger(25), 508)  - Power(BigInteger(5),400)  + Power(BigInteger(25), 250) - 27;
    var NumOfFours := 0;
    repeat 
      If (Num Mod 5) = BigInteger(4) Then Inc(NumOfFours);
      Num := Num div 5; // сдвигаем на один пятиричный разряд вправо
    until Num.IsZero;
    
    
    writeln(NumOfFours);    
  End;

{
 	(№ 3508) (Е. Джобс) Значение арифметического выражения: N^25 – 2·N^13 + 10 записали в системе счисления с основанием N. 
 	Определите основание системы счисления, если известно, что сумма разрядов в числе, представленном в этой системе счисления, равна 75.
}
  Begin
    writeln('------------------------');

    for var Base := 2 to 16 do
      begin
        var Num : BigInteger := Power(BigInteger(Base), 25) - 2 * Power(BigInteger(Base), 13) + 10;
        var SumOfDigits := 0;
        repeat 
          SumOfDigits += integer(Num Mod Base); // Num у нас типа BigInteger, поэтому приходится остаток от деления принудительно преобразоывать в integer, или надо было сразу SumOfDigits как BigInteger объявлять
          Num := Num div Base; // сдвигаем на один Base-ричный разряд вправо
        until Num = BigInteger(0);
        
        if SumOfDigits = 75  then Writeln(Base);
        
      end;
      
  End;  

{
(№ 2719) (mcko.ru) Запись некоторого натурального числа X в девятеричной системе счисления имеет ровно три значащих разряда и содержит хотя бы одну цифру 3. 
Это число увеличили в три раза, и оказалось, что запись получившегося числа Y в девятеричной системе также имеет ровно три значащих разряда. 
Чему равна сумма минимально возможного и максимально возможного чисел X? Ответ приведите в девятеричной системе счисления.
}
  
  Begin
      
    writeln('------------------------');
    
    var NumList := new List<integer>;
    
    for var d0 := 0 to 8 do
      for var d1:= 0 to 8 do
        for var d2:= 1 to 8 do // в старшем разряде не может быть ноль
          if (d0 = 3) or (d1 = 3) or (d2 = 3)
            then
              begin              
                var Num := d0 + d1*9 + d2*9*9; // тупо переводим из девятиричной системы
                if Num * 3 in [1*9*9 .. 9*9*9-1] // то есть число в диапазоне 100..888 в девятиричной системе
                  then NumList.Add(Num);
              end;
    
    
    var ResNum := NumList.Max + NumList.Min;
    var ResStr := '';
    repeat
      ResStr := Format('{0}', ResNum mod 9) + ResStr;  // ResStr := (ResNum mod 9).ToString + ResStr; // можно и так число в строку преобразовать
      ResNum := ResNum div 9; // сдвигаем на разряд вправо
    until ResNum = 0;
    
    writeln(ResStr);
    
  end;  
{
(№ 2233) (М.В. Кузнецова) Значение арифметического выражения: 9^5 + 3^7 – 14 записали в системе счисления с основанием 3. 
Какая из цифр реже всего встречается в этой записи? В ответе укажите, сколько таких цифр в записи. 
}  
  Begin
      
    writeln('------------------------');
  
    var Num : BigInteger := Power(BigInteger(9), 5) + Power(BigInteger(3), 7) - 14;
    var DigCount : array [0..2] of integer := (0, 0, 0);
    Repeat
      DigCount[integer(Num mod 3)] += 1;
      Num := Num div 3;
    until Num.IsZero;
    
    Writeln(DigCount); 
    
  end;    
end.