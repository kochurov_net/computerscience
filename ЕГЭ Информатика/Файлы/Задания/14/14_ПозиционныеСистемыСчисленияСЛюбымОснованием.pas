﻿Program Task14; //Раздел № 60: Позиционные системы счисления с любыми основаниями



Function NumToList(Num, Base : integer) : List<integer>;
Begin
  Result := new List<integer>;
  Repeat
    Result.Add(Num mod Base);
    Num := Num div Base;
  until Num = 0;
end;

function ListToNum(DigitsList : List<integer>; Base : integer) : integer;
Begin

  Result := 0;

  for var i : integer  := DigitsList.Count -1  downto 0 do // со старшего разряда начинаем
    begin 
      Result *= Base; // сдвигаем влево на разряд
      Result += DigitsList[i]; // добавляем цифру в младший разряд
    end;  

end;

Function ListOfDigitsToString(DigitsList : List<integer>) : string;
Begin
  var DigitsString := '0123456789ABCDEF';
  Result := '';
  Foreach var Dig in DigitsList do Result := DigitsString[Dig + 1] + Result; // символ добавляется в начало строки
end;

Begin
{
(№ 3656) (Б.С. Михлин) Число 456 записали в системах счисления с основаниями от 2 до 10 включительно. При каком основании количество нечётных цифр в записи этого числа будет максимальным? 
Если таких оснований несколько, то укажите максимальное из них. 
}  
  for var Base := 2 To 10 do 
    Begin
      var DigitsList := NumToList(456, Base);
      var NumOfOddDigits := 0;
      for var i:=0 to DigitsList.count - 1 do if Odd(DigitsList[i]) Then Inc(NumOfOddDigits);
      WritelnFormat('{0} {1} {2}', Base, NumOfOddDigits, ListOfDigitsToString(DigitsList));
    End;
    
{
 	(№ 3564) (Е.А. Мирончик) Некоторое число X из десятичной системы счисления перевели в системы счисления с основаниями 16, 8. 
 	Часть символов при записи утеряна. Позиции утерянных символов обозначены символом *:

  X = *E (16) = 2*6 (8) .

Сколько чисел соответствуют условию задачи? 
} 
  Writeln('---------');   
  var Dig16 := NumToList(1 * 16 + 14, 16); // минимальное число 1E, 2 разряда
  for var i := 0 to 15 do 
    begin
      Dig16[1] := i; //меняем первый разряд
      var Num := ListToNum(Dig16, 16);
      var Dig8 := NumToList(Num, 8);
      if (Dig8.Count = 3) And (Dig8[0] = 6) And (Dig8[2] = 2) Then WritelnFormat('{0} {1}', ListOfDigitsToString(Dig16) , ListOfDigitsToString(Dig8));
    end;
  
  
{
(№ 328) Запись числа 67 в системе счисления с основанием N оканчивается на 1 и содержит 4 цифры. Укажите основание этой системы счисления N. 
}  
  Writeln('---------');     
  
  for var i:= 2 to 16 do WritelnFormat('{0} {1}', i, ListOfDigitsToString(NumToList(67, i)));
  
end.



