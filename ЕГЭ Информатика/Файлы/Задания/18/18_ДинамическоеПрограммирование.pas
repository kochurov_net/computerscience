﻿Program Task18;

{

(№ 2367) Квадрат разлинован на N×N клеток (1 < N < 17). Исполнитель Робот может перемещаться по клеткам, 
выполняя за одно перемещение одну из двух команд: вправо или вниз. По команде вправо Робот перемещается в соседнюю правую клетку, 
по команде вниз – в соседнюю нижнюю. При попытке выхода за границу квадрата Робот разрушается. 
Перед каждым запуском Робота в каждой клетке квадрата лежит монета достоинством от 1 до 100. Посетив клетку, Робот забирает монету с собой; это также относится к начальной и конечной клетке маршрута Робота.
Исходные данные записаны в файле 18-10.xls https://kpolyakov.spb.ru/cms/files/ege-dynxls/18-10.xls в виде электронной таблице размером N×N, каждая ячейка которой соответствует клетке квадрата. Определите максимальную и минимальную денежную сумму, которую может собрать Робот, пройдя из левой верхней клетки в правую нижнюю. В ответе укажите два числа – сначала максимальную сумму, затем минимальную. 
}


const NumOfColumns = 10;
const NumOfRows = 10;

Var SourceArr : array [1..NumOfRows, 1..NumOfColumns] of integer := // скопировал таблицу из Либры, и заменил табуляцию на запятую. АХТУНГ: ПаскальАБЦ не умеет заменять в выделенном блоке, надо замену делать в отдельном файле, чтобы ничего не испортить
  (
    (63,78,58,93,49,83,92,3,51,57),
    (10,1,42,24,55,59,66,48,76,79),
    (25,29,87,76,99,63,32,22,87,48),
    (88,40,65,9,86,38,56,31,46,95),
    (79,91,77,62,60,73,90,44,41,51),
    (47,39,73,7,68,4,91,32,75,44),
    (59,65,23,87,58,93,64,34,1,64),
    (42,96,69,33,83,8,37,41,37,91),
    (49,27,94,18,89,55,31,97,62,92),
    (25,68,71,13,67,83,37,22,13,8)
  );

type TRowCol = array of integer;

Function GetSetOfNeighbors(Row, Col : integer) : set of TRowCol;
Begin
  Result := [];
  if Col -1 in [1..NumOfColumns] Then Result += [Arr(Row, Col - 1)]; // мог прийти слева
  if Row -1 in [1..NumOfRows] Then Result += [Arr(Row - 1, Col)]; // мог прийти сверху
end;


var LargerIsBetter := true;

Function FirstNumIsBetter(FirstNum, SecondNum : integer) : boolean;
Begin
  if LargerIsBetter Then Result := FirstNum > SecondNum Else Result := FirstNum < SecondNum;
End;  
  

Function GetOptimalNeihbor(SetOfNeighbors : Set of TRowCol) : TRowCol;
Begin
  var OptRow, OptCol : integer; 
  var IsFirstItem := True;
  ForEach var RowCol : TRowCol in SetOfNeighbors do
    begin
      if IsFirstItem 
        Then Begin OptRow := RowCol[0]; OptCol := RowCol[1]; IsFirstItem := False; End
        Else If FirstNumIsBetter(SourceArr[RowCol[0], RowCol[1]],  SourceArr[OptRow, OptCol]) Then begin OptRow := RowCol[0]; OptCol := RowCol[1]; end;
    end;
    
    Result := Arr(OptRow, OptCol);
    
end;

Begin
  
    Println(SourceArr);
 
    //LargerIsBetter := false;  // если раскомментировать, то найдет минимум
   
    For var Row := 1 to NumOfRows do        
       For var Col := 1 to NumOfColumns do
        begin
          Var SetOfNeighbors := GetSetOfNeighbors(Row, Col);
          if SetOfNeighbors <> []
            Then 
              Begin
                var OptRowCol  := GetOptimalNeihbor(SetOfNeighbors);
                SourceArr[Row, Col] += SourceArr[OptRowCol[0], OptRowCol[1]];               
              end;
        end;
    Writeln;
    Println(SourceArr);
    
    Writeln;
    Println(SourceArr[NumOfRows, NumOfColumns]);
    
end.