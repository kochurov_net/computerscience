﻿Program Task25;
{
(№ 2928) Напишите программу, которая ищет среди целых чисел, принадлежащих числовому отрезку [126849; 126871], 
числа, имеющие ровно 4 различных делителя. 
Выведите для каждого найденного числа два наибольших делителя в порядке возрастания
}

{
Идея решения описана в файле http://kpolyakov.spb.ru/download/ege25.doc
Суть идеи в том, что у любое число можно разложить на произведение простых чисел
Два делителя - это 1 и само число, следовательно, два других делителя - это простые числа, причем разные
два наибольших делителя в продядке возрастания - это пары - максимальный из простых делителей и само число
}

function IsPrime(Num: integer) : boolean;
Begin
   
   Result := Num > 1; // при таком условии имеет смысл перебирать делители
   
   Var Divider := 2;
   
   While (Sqr(Divider) <= Num) And Result do // пытаемся делить, пока квадрат делителя меньше делимого и число не делится без остатка
     begin
        Result := (Num mod Divider <> 0);
        Inc(Divider);
     end;

end;

Begin
  
  for var Num: integer := 126849 To 126871 Do   
    begin
      for var Divider := 2 To Round(sqrt(Num)) do
        if IsPrime(Divider) Then if Num mod Divider = 0 
          Then
            begin
              var Divider2 := Num div Divider;
              if IsPrime(Divider2) And (Divider <> Divider2) Then Writeln(Max(Divider, Divider2), ' ', Num);
            end;
           
    end  
end.