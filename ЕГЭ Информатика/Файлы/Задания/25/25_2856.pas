﻿Program Task25;
{
(№ 2856) (Е. Джобс) Среди целых чисел, принадлежащих числовому отрезку [333555; 777999], найдите числа, среди делителей которых есть ровно 35 двузначных чисел. 
Для каждого найденного числа запишите в ответе само число, наименьший и наибольший из его двузначных делителей. Так, например, для числа 36 учитываются только делители 12 и 18. 
}

function GetTwoDigitDividers(Num : longword) : List<word>;
Begin
  Result := new List<word>;
  for var Divider := 10 To 99 Do // предполагаем, что диапазон делителей и делимых не пересекается
    if (Num Mod Divider = 0) Then Result.Add(Divider);
end;

Begin
  
  for var Num: longword := 333555 To 777999 Do
    begin
      var Divs := GetTwoDigitDividers(Num);
      if Divs.Count = 35 Then Writeln(Num, ' ', Divs.Min(), ' ', Divs.Max());
    end;

end.