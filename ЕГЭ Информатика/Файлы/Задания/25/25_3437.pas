﻿Program Task25;
{
(№ 3437) (А. Рулин) Рассматривается множество целых чисел, принадлежащих числовому отрезку [862346; 1056242]. 
Найдите числа, нетривиальные делители которых образуют арифметическую прогрессию с разностью d = 100. 
В ответе для каждого такого числа (в порядке возрастания) запишите сначала само число, а потом – его максимальный нетривиальный делитель. 
}

function GetDividers(Num : integer) : List<integer>; // получаем делители за исключением 1 и самого числа
Begin
  Result := New List<integer>;  
  For var Divider : integer := 2 To Round(Sqrt(Num)) Do
    if (Num Mod Divider = 0) 
      Then
        Begin
          Result.Add(Divider);
          If Sqr(Divider) <> Num Then Result.Add(Num div Divider);
        end;
  
end;

Begin
  
  for var Num: integer := 862346 To 1056242 Do   
    begin
      
      Var Divs := GetDividers(Num);
      Divs.Sort(); // сортируем делители
      
      if Divs.Count < 2 Then Continue; // меньше двух делителей - нет смысла проверять прогрессию
      
      var ProgressionOK := True;
      For var i := 1 To Divs.Count-1 do // начинаем цикл с 1, то есть со второго элемента списка, первый имеет индекс 0
        ProgressionOK := ProgressionOK And ( Divs[i] - Divs[i-1] = 100);
      
      if ProgressionOK Then Writeln(Num, ' ', Divs.Max);  
    
    end;
end.