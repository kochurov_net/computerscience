﻿Program Task25;
{
(№ 3657) (С.О. Куров) Среди целых чисел, принадлежащих числовому отрезку [1000000; 1300000], 
найдите числа, у которых все цифры меньше тройки, а сумма цифр кратна десяти. 
Среди всех таких чисел необходимо отобрать каждое десятое (10-е, 20-е, 30-е и т.д.). 
Расположите найденные числа в порядке возрастания, справа от каждого числа укажите количество его собственных делителей (не равных 1 и самому числу). 
}


function IsNumOK(Num : integer) : boolean;
Begin
  Result := true;
  var SumOfDigits := 0;
  repeat
    var Digit := Num mod 10;
    Result := (Digit < 3);
    Num := Num div 10;
    SumOfDigits += Digit;
  until Not Result Or (Num = 0);
  
  //Result := Result And SumOfDigits.Divs(10);  
  Result := Result And (SumOfDigits mod 10 = 0) ;
  
end;

function CountOfDividers(Num : integer) : integer;
Begin
  Result := 0;
  var Divider := 2; // игнорируем делители 1 и Num
  While sqr(Divider) <= Num Do
    begin
      if Num mod Divider = 0 
        Then if sqr(Divider) = Num 
          Then Inc(Result, 1)  // добавляем один делитель  
          Else Inc(Result, 2); // добавляем два делителя
      Inc(Divider);
    end;
end;

Begin
  var NumCount := 0; 
  for var Num: integer := 1000000 To 1300000 Do   
    begin
      if IsNumOK(Num) 
        Then 
          begin
            Inc(NumCount); 
            if ( NumCount mod 10 = 0 ) Then Writeln(Num, ' ', CountOfDividers(Num));
          end;  
    end;  
end.