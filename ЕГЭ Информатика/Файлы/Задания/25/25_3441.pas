﻿Program Task25;
{
(№ 3441) (Е. Джобс) Для интервала [33333;55555] найдите числа, которые кратны сумме своих простых делителей. 
В качестве ответа приведите числа, для которых сумма простых делителей больше 250, – сначала найденное число, затем сумму его простых делителей. 
Примечание: само число в качестве делителя не учитывается. 
}

function IsPrime(Num: integer) : boolean;
Begin
   
   Result := Num > 1; // при таком условии имеет смысл перебирать делители
   
   Var Divider := 2;
   
   While (Sqr(Divider) <= Num) And Result do // пытаемся делить, пока квадрат делителя меньше делимого и число не делится без остатка
     begin
        Result := (Num mod Divider <> 0);
        Inc(Divider);
     end;

end;

Begin
  
  var PrimeNums := new List<integer>;
  for Var i:= 2 To 55555 Do 
    If IsPrime(i) Then PrimeNums.Add(i); // получаем список простых чисел
  
  
  for var Num: integer := 33333 To 55555 Do   
    begin

      var DivSum := 0;

      Foreach Var Divider In PrimeNums do 
        If (Divider < Num) And (Num mod Divider = 0) Then Inc(DivSum, Divider);
        
      if (DivSum > 250) And (Num mod DivSum = 0) Then Writeln(Num, ' ', DivSum);
        
    end  
end.