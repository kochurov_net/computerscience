﻿Program Task25;
{
(№ 2908) (Б.С. Михлин) Напишите программу, которая ищет среди целых чисел, принадлежащих числовому отрезку [586132; 586430], числа, 
имеющие максимальное количество различных делителей. 
Найдите минимальное и максимальное из таких чисел. 
Для каждого из них в отдельной строчке выведите количество делителей и наибольший делитель, не равный самому числу.
}

function GetDividers(Num : longword) : List<longword>; // получаем делители за исключением 1 и самого числа
Begin
  Result := New List<longword>;  
  For var Divider : longword := 2 To Round(Sqrt(Num)) Do
    if (Num Mod Divider = 0) 
      Then
        Begin
          Result.Add(Divider);
          If Sqr(Divider) <> Num Then Result.Add(Num div Divider);
        end;
  
end;

Begin
  Var MaxNumOfDividers := 0; // максимальное число делителей
  
  var Nums := new List<longword>; // список чисел с максимальным числом делителей

  for var Num: longword := 586132 To 586430 Do   
    begin
      
      Var NumOfDividers := GetDividers(Num).Count + 2; // добавляем единицу и само число в количество делителей
      
      If NumOfDividers < MaxNumOfDividers Then Continue;
      
      If NumOfDividers > MaxNumOfDividers
        Then 
          Begin
            MaxNumOfDividers := NumOfDividers;
            Nums.Clear;
            Nums.Add(Num);
          end
        Else Nums.Add(Num);
              
    end;
    
    Writeln(MaxNumOfDividers, ' ', GetDividers(Nums.Min()).Max());
    Writeln(MaxNumOfDividers, ' ', GetDividers(Nums.Max()).Max());

end.