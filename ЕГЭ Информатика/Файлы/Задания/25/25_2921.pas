﻿Program Task25;

{
(№ 2921) Напишите программу, которая ищет среди целых чисел, принадлежащих числовому отрезку [193136; 193223], 
числа, имеющие ровно 6 различных делителей. Выведите для каждого найденного числа два наибольших делителя в порядке возрастания. 
}

{
Идея решения описана в файле http://kpolyakov.spb.ru/download/ege25.doc
Суть идеи в том, что у любое число можно разложить на произведение простых чисел
гипотеза - два разных простых числа с квадратами
тогда 
делители: 1, ч1, ч2, ч1 * ч2, ч1^2 * ч2, ч2^2 * ч1, ч1^2 * ч2^2
7 делителей, гипотеза неверна
гипотеза - два простых числа, одно с квадратом
тогда 
делители: 1, ч1, ч2, ч1 * ч2, ч1^2, ч1^2 * ч2
6 делителей

два наибольших делителя в продядке возрастания - это пары - исходное число, деленное на минимальный простой делитель и само число
Делители ищем следующим образом: 
- ищем первый простой делитель, проверяем, является частное квадратом простого числа
- если нет, проверяем, является ли частное от деления числа на квадрат найденного делителя простым числом
}

function IsSquare(Num: integer) := Num = round(Sqrt(Num));

function IsPrime(Num: integer) : boolean;
Begin
   
   Result := Num > 1; // при таком условии имеет смысл перебирать делители
   
   Var Divider := 2;
   
   While (Sqr(Divider) <= Num) And Result do // пытаемся делить, пока квадрат делителя меньше делимого и число не делится без остатка
     begin
        Result := (Num mod Divider <> 0);
        Inc(Divider);
     end;

end;

function IsPrimeDivider(Num, Divider : integer) := (Num mod Divider = 0) And IsPrime(Divider);

Begin
  
  for var Num: integer := 193136 To 193223 Do   
    begin
            
      for var Divider := 2 To Round(sqrt(Num)) do
        if IsPrimeDivider(Num, Divider)
          Then
            begin
              var Quotient := (Num div Divider);
              var Divider2 : integer;
              if (IsSquare(Quotient) And IsPrime(Round(Sqrt(Quotient)))) // если частное - квадрат простого числа
                Then Divider2 := Round(Sqrt(Quotient))
                Else If (Num mod Sqr(Divider) = 0) And IsPrime(Num div Sqr(Divider)) // иначе проверяем, является ли результат деления числа на квадрат найденного простого делителя простым числом
                  Then Divider2 := Num div Sqr(Divider)
                  Else Continue;
              
              writeln(Num / Min(Divider, Divider2), ' ', Num);
              
            end;
           
    end  
end.