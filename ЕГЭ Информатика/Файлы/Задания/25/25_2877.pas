﻿Program Task25;
{
 	(№ 2877) (П.Е. Финкель) Уникальным назовём число, если у него только первые две цифры нечётные. 
 	Для интервала [57888;74555] найдите количество таких чисел, которые не делятся на 7, 9, 13, и разность максимального и минимального из них. 
 	В ответе укажите два числа: сначала количество чисел, а потом разность.
}

Function IsNumOK(Num : longword) : boolean;
Begin
  
  var TwoDigitsNum := Num;
  Result := True;
  While (TwoDigitsNum >= 100) do 
    begin
      Result := Result And Not Odd(TwoDigitsNum mod 10); // выкидываемые цифры все должны быть четные
      TwoDigitsNum := TwoDigitsNum div 10;
    end;
   
  Result := Result And Odd(TwoDigitsNum) And Odd(TwoDigitsNum div 10) And (Num mod 7 <> 0) And (Num mod 9 <> 0) And (Num mod 13 <> 0);
  
end;

Begin
  
  var Nums := New List<longword>;
  
  for var Num: longword := 57888 To 74555 Do
      if IsNumOK(Num) Then Nums.Add(Num);
  
  Writeln(Nums.Count, ' ', Nums.Max() - Nums.Min());
  
end.