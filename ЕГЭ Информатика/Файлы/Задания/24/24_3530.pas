﻿Program Task24;
{
(№ 3530) (А. Кабанов) Текстовый файл 24-153.txt содержит строку из заглавных букв A, B, C, D, E, F, всего не более чем из 10^6 символов. 
AF-подстроками назовём непрерывные непустые последовательности символов, начинающиеся символом A, и заканчивающиеся символом F (граничные символы входят в подстроку). 
Определите количество AF-подстрок длиной от 7 до 10 символов. 
Ответ: 3703
}
 
begin
  var Str : string;
  Assign(input, '24-153.txt');
  Readln(Str);

  var Count : integer := 0;
      
  for var i := 1 to Str.Length - 1 do
    Begin
      
     if Str[i] <> 'A' Then continue;  // крутим цикл,пока не нашли A
     
     var AFStr := '';
      
     for var j := i to Str.Length do // с того места, где нашли A, начинаем собирать строки, до тех пор, пока не встретится F
        Begin
          
          AFStr += Str[j];
          
          if AFStr.Length > 10 Then Break; // длина собранной строки больше 10 - нет смысла дальше смотреть
          
          if Str[j] = 'F'
            Then if AFStr.Length in [7..10] Then Inc(Count); // если собранная строка удовлетворяет условию, считаем ее
        
        End;
      
    End;
    
    writeln(Count);
    
end.