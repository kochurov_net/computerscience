﻿Program Task24;

{
(№ 3359) (В.Н. Шубинкин) Текстовый файл 24-2.txt https://kpolyakov.spb.ru/cms/files/ege-sym/24-2.txt  содержит последовательность из строчных и заглавных букв английского алфавита и цифр, 
всего не более 10^6 символов. Возрастающей подпоследовательностью будем называть последовательность символов, расположенных в порядке увеличения их номера в кодовой таблице символов ASCII. 
Определите длину наибольшей убывающей подпоследовательности. 
}

Begin
  
  var Str: string;
  
  Assign(input,'24-2.txt');
  Readln(Str);
  
  var SeqLen    : integer := 1;
  var MaxSeqLen : integer := 1;

  for var i:= 2 to Str.Length do
    if Str[i] < Str[i-1] 
      Then Inc(SeqLen) 
      Else 
        Begin 
          if SeqLen > MaxSeqLen Then MaxSeqLen := SeqLen; 
          SeqLen := 1;
         End;

  Writeln(MaxSeqLen);

End.  