﻿Program Task16;

function F(n: integer): 
              integer;
begin
  if n < 5 then
    F := F(n+3) + 
         F(2*n) + 
         F(3*n div 2)
  else
    F := n + 2;
end;

Begin
  Writeln(F(3));
end.