﻿// переписал под 2.2, но не работает :(

{
(№ 3078) (А. Кабанов) Два игрока, Петя и Ваня, играют в следующую игру. Перед игроками лежит куча камней. Игроки ходят по очереди, первый ход делает Петя. За один ход игрок может добавить в кучу два камня, добавить в кучу три камня или увеличить количество камней в куче в два раза. Например, имея кучу из 15 камней, за один ход можно получить кучу из 17, 18 или 30 камней. У каждого игрока, чтобы делать ходы, есть неограниченное количество камней. Игра завершается в тот момент, когда количество камней в куче становится не менее 30. Победителем считается игрок, сделавший последний ход, то есть первым получивший кучу, в которой будет 30 или больше камней.
В начальный момент в куче было S камней, 1 ≤ S ≤ 29.
Ответьте на следующие вопросы:
  Вопрос 1. Найдите минимальное значение S, при котором Ваня выигрывает своим первым ходом при любой игре Пети.
  Вопрос 2. Сколько существует значений S, при которых у Пети есть выигрышная стратегия, причём одновременно выполняются два условия:
− Петя не может выиграть за один ход;
− Петя может выиграть своим вторым ходом независимо от того, как будет ходить Ваня. 
  Вопрос 3. Найдите два значения S, при которых одновременно выполняются два условия:
– у Вани есть выигрышная стратегия, позволяющая ему выиграть первым или вторым ходом при любой игре Пети;
– у Вани нет стратегии, которая позволит ему гарантированно выиграть первым ходом. 
Найденные значения запишите в ответе в порядке возрастания.   
1) 13
2) 4
3) 8 9 
}

Program Task1921;

type TStatesEnum = (all, any, win, w21);

function GetVariantsOfMove(StonesNum: integer) := Arr(StonesNum + 2, StonesNum + 3, StonesNum * 2);

function IsWin(StonesVariants: array of integer) : boolean;
Begin
  Result := StonesVariants.Any(x -> x >= 30);
End;  

function DoMove(StonesVariants: array of integer; States : sequence of TStatesEnum) : boolean;

Begin
  if (States.Count > 1) and IsWin(StonesVariants) then result := false
  Else
    case States.First of
      win: result := IsWin(StonesVariants);
      all: result := StonesVariants.all(x-> DoMove(GetVariantsOfMove(x), States.Skip(1)));
      any: result := StonesVariants.any(x-> DoMove(GetVariantsOfMove(x), States.Skip(1)));
      w21: 
        Begin
          var i := StonesVariants.Count(x-> DoMove(GetVariantsOfMove(x), States.Skip(1)));
          var j := StonesVariants.Count(x-> DoMove(GetVariantsOfMove(x), States.Skip(3)));
          Result := (i*j > 0) And (i+j = StonesVariants.Count);
        end;
    end;
end;

Begin
  PrintLn(Range(1,29).Where(x-> DoMove(GetVariantsOfMove(x), Seq(all, win))).Min);
  PrintLn(Range(1,29).Where(x-> DoMove(GetVariantsOfMove(x), Seq(any, all, win))).Count);
  PrintLn(Range(1,29).Where(x-> DoMove(GetVariantsOfMove(x), Seq(w21, any, all, win))));
end.