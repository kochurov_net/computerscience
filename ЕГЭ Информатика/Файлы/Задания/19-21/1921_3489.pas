﻿{
 (№ 3489) (А. Кабанов) Два игрока, Петя и Ваня, играют в следующую игру. Перед игроками лежит две кучи камней. 
 Игроки ходят по очереди, первый ход делает Петя. 
 За один ход игрок может убрать из кучи один камень или уменьшить количество камней в любой куче в два раза 
 (если количество камней нечётно, то остаётся на один камень меньше, чем убирается). 
 Игра завершается в тот момент, когда общее количество камней в двух кучах становится не более 18, побеждает игрок, сделавший последний ход. 
 В начальный момент в первой куче было K≥1 камней, а во второй – S≥1 камней, S+K ≥ 19.
Ответьте на следующие вопросы:
  Вопрос 1. Известно, что из начальной позиции (M; M) Ваня выигрывает первым ходом при любой игре Пети. При каком значении M это возможно?
  Вопрос 2. При K=13, найдите минимальное и максимальное значение S, при котором у Пети есть выигрышная стратегия, причём одновременно выполняются два условия:
− Петя не может выиграть за один ход;
− Петя может выиграть своим вторым ходом независимо от того, как будет ходить Ваня.
Найденные значения запишите в ответе в порядке возрастания.
  Вопрос 3. При каком минимальном значении N для начальной пары (N;N) одновременно выполняются два условия:
– у Вани есть выигрышная стратегия, позволяющая ему выиграть первым или вторым ходом при любой игре Пети;
– у Вани нет стратегии, которая позволит ему гарантированно выиграть первым ходом.  

1) 13
2) 14 27
3) 14 

}


{
  Информатик БУ: https://www.youtube.com/watch?v=Hp313trVFjc
}
Program Task1921;

type TRowCol = array of integer;

function GetVariantsOfMove(StonesNums: array of integer) := Arr(
      Arr(StonesNums[0] - 1, StonesNums[1]),
      Arr(StonesNums[0], StonesNums[1] - 1),
      Arr(StonesNums[0] - (StonesNums[0] div 2) - (StonesNums[0] mod 2), StonesNums[1]),
      Arr(StonesNums[0], StonesNums[1] - (StonesNums[1] div 2) - (StonesNums[1] mod 2))
      ); // получаем массив возможных состояний кучи в зависимости после каждого из возможных ходов

function IsWin(StonesVariants : array of array of integer) : boolean;
Begin 
  Result := StonesVariants.Any(x -> (x.Sum <= 18) ); // истина, если одно из состояний удовлетворяет критерию выигрыша
End;  

var SSeq := Range(35,1,-1); // АХТУНГ: когда камни из куч убирают, надо крутить цикл от большего значения к меньшему  // при 40 виснет

Begin
 
  var Table := new integer[SSeq.Max + 1, SSeq.Max + 1]; // таблица ходов, +1 потому что индексы в динамическом массиве начинаются с нуля    
 
  var Keys := new List<TRowCol>; // а в 2.2 будем перебирать
  foreach var r in SSeq do
    foreach var c in Sseq do
      Keys.Add(Arr(r,c)); // добавляем в список пару строка/колонка
  
  
  foreach var i in Keys do if IsWin(GetVariantsOfMove(i)) // заполняем словарь значениями: 
    Then Table[i[0], i[1]] := 1 //- единица - первый игрок выиграл первым ходом,
    Else Table[i[0], i[1]] := 0; // ноль - пока неизвестно, будем дальше уточнять
                                                                                 // ноль - пока неизвестно, будем дальше уточнять
  repeat  
    
    var SelectedKeys := Keys.Where(x -> Table[x[0], x[1]] = 0);
    
    if SelectedKeys.Count = 0 Then Break;
    
    foreach var i in SelectedKeys do // перебираем элементы только с нулевыми значениями
      begin
        Var Variants := GetVariantsOfMove(i).Select(x -> Table[x[0], x[1]]); // получаем варианты ходов: сначала индексы в словаре, а потом из словаря значения по этим индексам
        if Variants.All(x -> x > 0) Then Table[i[0], i[1]] := -Variants.Max(); // из всех позиций выигрывает первый игрок, ставим в таблицу второму игроку (с минусом) проигрыш с максимальнным номером хода
        if Variants.Any(x -> x < 0) Then Table[i[0], i[1]] := -Variants.Min() + 1; // существует позиция, из которой выигрывает второй игрок, 
                                                                                 // ставим первому игроку проигрыш (минус на минус) с максимальным номером (минимальный среди отрицательных) плюс один
      end; 
      
  until false;  // крутим цикл до тех пор пока не осталось состояний с неизвестным результатом, то есть до заполнения всей таблицы    

  //Table.Println;
  SSeq.Where(x -> Table[x,x] = -1).Println; // Второй игрок выигрывает первым ходом

  var ResTable := SSeq.Where(x -> Table[13,x] = 2); // Первый игрок выигрывает вторым ходом 
  writeln(ResTable.Min, ' ', ResTable.Max);

  Println(SSeq.Where(x -> Table[x,x] = -2).Min);  // Второй игрок выигрывает вторым ходом 

end.

{
O. (13, 26)
1. (12,26) (13,25) (6,26) (13,13)
2. (11,26) (12,25) (6,26) (12,13) - (12,25) (13,24) (6,25) (13,12) - (5,26) (6,25) (3,26) (6,13) - (12,13) (13,12) (6,13) (13,6)
3. первый игрок выигрывает вторым ходом - Кабанов считает, что максимальное значение равно, при котором можно выиграть, равно 27, а у нас программа его не выдает
! а не выдает потому что мы крутили цикл по возрастанию, а надо было по убыванию крутить

}