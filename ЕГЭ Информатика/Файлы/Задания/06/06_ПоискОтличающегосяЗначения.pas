﻿Program Task6;
{

(№ 1794) (А.Г. Минак) Определите, при каком наименьшем положительном введённом значении переменной s программа выведет число s, отличающееся от введенного значения.

var s, n: integer;
begin
  readln (s);
  n := 100;
  while s - n >= 100 do
  begin
    s := s + 20;
    n := n + 40
  end;
  writeln(s)
end.

}

function Func(s: integer) : integer;
Begin
  var n := 100;
  while s - n >= 100 do
  begin
    s := s + 20;
    n := n + 40
  end;
  
  Func := s;

end;

Begin
 
 var s := 0;
 Repeat
   Inc(s);
 until s <> Func(s);
 
 Writeln(s);
 
end.
