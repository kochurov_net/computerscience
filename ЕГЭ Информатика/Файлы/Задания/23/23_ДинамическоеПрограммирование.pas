﻿Program Task23;
{
(№ 2453) Исполнитель Калькулятор преобразует число на экране. У исполнителя есть две команды, которым присвоены номера:

1. Прибавить 1
2. Умножить на 2

Программа для исполнителя Калькулятор – это последовательность команд. Сколько существует программ, для которых при исходном числе 1 результатом является число 20, и при этом траектория вычислений содержит число 10? 
}

Function DoCommand(Num: integer; Command: integer) : integer;
Begin
  Case Command Of
    1 : Result := Num + 1;
    2 : Result := Num * 2;
  end;
end;

var Count : integer = 0;

function GetSequences(Res : integer; StopNum : integer) : integer;
Begin
  Result := 0; 
  if Res = StopNum 
    Then Result := 1 //Inc(Count)
    Else 
      if Res < StopNum 
        Then 
           foreach var Command : integer in [1, 2] do Result += GetSequences( DoCommand(Res, Command), StopNum );
end;

Begin 
  
  Writeln(GetSequences(1,10) * GetSequences(10,20));
  
end.