﻿Program Task22;
{
№ 3012) Получив на вход число х, этот алгоритм печатает два числа К и R. 
Укажите наименьшее из таких чисел х, при вводе которых алгоритм печатает сначала 4, а потом 3.

var x, i, K, R, y: longint;
begin
  readln(x);
  K := 0; R := 9;
  y := x mod 10; 
  while x > 0 do begin
    K := K + 1;
    if R > x mod 10 then
      R := x mod 10;
    x := x div 10
  end;
  R := y - R; 
  writeln(K); writeln(R)
end.

}

function F(x : longint) : array of longint;
//var x, i, K, R, y: longint;
var i, K, R, y: longint;
begin
  //readln(x);
  K := 0; R := 9;
  y := x mod 10; 
  while x > 0 do begin
    K := K + 1;
    if R > x mod 10 then
      R := x mod 10;
    x := x div 10
  end;
  R := y - R; 
  //writeln(K); writeln(R)
  
  Result := Arr (K, R); // в 2.2 нет кортежей, возвращаем массив
  
end;


Begin
  //for var x :longint := longint.MinValue to longint.MaxValue do // так можно написать, но работать полный перебор будет довольно долго.
  for var x :longint := 0 to longint.MaxValue do // доказали, что при х меньше равно нулю цикл в функции не выполнится ни разу и K у нас всегда будет равен нулю
    Begin 
      var KR := F(x);
      if (KR[0] = 4) And (KR[1] = 3)
          Then
            Begin 
              Writeln(x);
              Break;
            end;
    End;        
end.