﻿
Var Paths := Dict (  
  'А' => 'БГД',   
  'Б' => 'ВГ',  
  'В' => 'ЖИ',   
  'Г' => 'ВЖЕ',   
  'Д' => 'ГЗ',
  'Е' => 'ЖЗ',     
  'Ж' => 'ИКЛЗ',     
  'З' => string('Л'),  
  'И' => 'КОМ',   
  'К' => 'ЛО',   
  'Л' => string('О'),
  'М' => 'ТН',   
  'Н' => string('Т'),
  'О' => 'МН',
  'Т' => ''
  );

var Count := 0;

function IsPathOK(var StrPath : string) := StrPath.EndsWith('Т') And StrPath.Contains('И');

procedure GetPath(StrPath : string);
Begin 
  if IsPathOK(StrPath)
    then 
      Begin
        Count += 1;
        Writeln(StrPath);
      End
    else 
      Begin
        var NextPoints := Paths[StrPath[StrPath.Length]];
        foreach var NextPoint in NextPoints do GetPath(StrPath + NextPoint);             
      End;  
End;


Begin 
  
  GetPath('А');
  
  Writeln(Count);
 
End.  