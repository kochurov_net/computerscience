﻿Program Task12;

{
(№ 2101) Исполнитель Редактор получает на вход строку цифр и преобразовывает её. Редактор может выполнять две команды, в обеих командах v и w обозначают цепочки цифр.

1. заменить (v, w)
2. нашлось (v)

Первая команда заменяет в строке первое слева вхождение цепочки v на цепочку w, вторая проверяет, встречается ли цепочка v в строке исполнителя Редактор. Если она встречается, то команда возвращает логическое значение «истина», в противном случае возвращает значение «ложь». Дана программа для исполнителя Редактор:

НАЧАЛО
  ПОКА нашлось (11)
    ЕСЛИ нашлось(112)
      ТО заменить (112, 5)
      ИНАЧЕ заменить (11, 3)
  КОНЕЦ ПОКА
КОНЕЦ

Исходная строка содержит 23 единицы и 5 двоек, других цифр нет, точный порядок расположения цифр неизвестен. Какую наибольшую сумму цифр может иметь строка, которая получится после выполнения программы? 
}

{
Кочуров А.Л. - полным перебором за адекватное время не решается, вот заготовки функций
Решение вручную:
Максимальная сумма - 5 чисел 112 = 5 двоек и 10 единиц, заменяются на 5 чисел 5, сумма = 25
остается 23 - 10 = 13 единиц, их можно заменить на 6 чисел 3, сумма 18
и еще одна единичка, итого сумма 5*5 + 3*6 + 1 = 44
}

Function Func(Str: String) := Str.Replace('112','5').Replace('11','3');

Function SumOfDigits(Str: String) : integer;
Begin
  Result := 0;
  foreach var Digit in Str do Result += String(Digit).ToInteger;
end;

Var SetOfStrings : set of String = [];

Procedure GetPermutations(InitialStr: String; AvailableDigits: String);
Begin
  If AvailableDigits = ''
    Then SetOfStrings += [InitialStr]
    Else
      begin
        var PrevDig := '';
        for var i := 1 to AvailableDigits.Length do
        //ForEach var Dig in AvailableDigits Do 
          begin 
            if AvailableDigits[i] = PrevDig Then Continue Else PrevDig := AvailableDigits[i];
            
            GetPermutations(InitialStr + string(AvailableDigits[i]), AvailableDigits.Right(AvailableDigits.Length - 1));
          end;  
      end;  
end;

Begin
  Writeln(DateTime.Now);
  
//  var a := Arr(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 2,2,2,2,2);
//  foreach var b in a.Permutations do 
//    begin
//      var S : string = '';
//      Foreach var Item in b do S += Item.ToString;
//      //Writeln(DateTime.Now, S);
//    end;
  
  
  GetPermutations('',string(23*'1') + string(5*'2'));
  
  Writeln(DateTime.Now);
  
end.