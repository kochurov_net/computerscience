﻿Program Task17;

{
  (№ 2747) (А.Г. Минак) Рассматривается множество целых чисел, принадлежащих числовому отрезку [1005; 147870], которые не имеют в своей записи цифру 1, 
  и у которых разность между максимальной и минимальной цифрой меньше четырёх. 
  Найдите количество таких чисел и двадцать пятое по порядку такое число, если считать эти числа в порядке от наибольшего к наименьшему. 
  В ответе запишите два целых числа: сначала количество, затем двадцать пятое по порядку такое число, если считать эти числа от наибольшего к наименьшему. 
}

Function IsNumberOK(Num : integer) : boolean;
Begin
  Var ListOfDigits := new List<integer>;
  repeat 
    var CurDigit := Num mod 10;
    ListOfDigits.Add(CurDigit);
    Num := Num Div 10;
  until Num = 0;
  
  ListOfDigits.Sort(); // можно написать и Sort(ListOfDigits); 
  Result := Not ListOfDigits.Contains(1) And  (ListOfDigits[ListOfDigits.Count - 1] - ListOfDigits[0] < 4); // отсортировали список и получаем первый и последний его элементы
  // или так: 
  Result := Not ListOfDigits.Contains(1) And  (ListOfDigits.Max - ListOfDigits.Min < 4); // а можно не сортировать вообще, а сразу их при помощи специальных методов максимум и минимум получить
end;

Begin
  var ListOfNumbers := new List<integer>;
  
  For var Num := 147870 downto 1005  Do If IsNumberOK(Num) Then ListOfNumbers.Add(Num); // чтобы не сортировать по убыванию
  
  ListOfNumbers.Sort();    // но умеем и сортировать
  ListOfNumbers.Reverse(); // потом переворачиваем в обратном порядке
  
  WritelnFormat('{0} {1}', ListOfNumbers.Count, ListOfNumbers[25 - 1]);
  
end.